//
//  HomeViewModel.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 22/07/21.
//

import SwiftUI

final class HomeViewModel: ObservableObject{
    
    private let router: HomeRouter
    
    init(router: HomeRouter) {
        self.router = router
    }
    
}
