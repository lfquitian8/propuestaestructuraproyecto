//
//  HomeRouter.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 22/07/21.
//

import SwiftUI

final class HomeRouter{
    
    static func view()->HomeView{
        let router = HomeRouter()
        let viewModel = HomeViewModel(router: router)
        let view = HomeView(viewModel: viewModel)
        
        return view
    }
}
