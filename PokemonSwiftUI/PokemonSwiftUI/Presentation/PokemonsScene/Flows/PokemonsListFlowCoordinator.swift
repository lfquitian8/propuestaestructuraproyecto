//
//  PokemonsSearchFlowCoordinator.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import SwiftUI

protocol PokemonsListFlowCoordinatorDependencies  {
    func makePokemonsListView(actions: PokemonsListViewModelActions) -> PokemonsListView
}

final class PokemonsListFlowCoordinator {
    
    private let dependencies: PokemonsListFlowCoordinatorDependencies

    private var pokemonsListV: PokemonsListView?

    init(dependencies: PokemonsListFlowCoordinatorDependencies) {
        self.dependencies = dependencies
    }
    
    func start()-> PokemonsListView {
        // Note: here we keep strong reference with actions, this way this flow do not need to be strong referenced
        let actions = PokemonsListViewModelActions(showPokemonDetails: showPokemonDetails)
        let vcPokemon = dependencies.makePokemonsListView(actions: actions)
        pokemonsListV = vcPokemon
        return pokemonsListV!
    }

    private func showPokemonDetails(pokemon: Pokemon) {
//        let vc = dependencies.makePokemonsDetailsViewController(pokemon: pokemon)
//        navigationController?.pushViewController(vc, animated: true)
    }

}
