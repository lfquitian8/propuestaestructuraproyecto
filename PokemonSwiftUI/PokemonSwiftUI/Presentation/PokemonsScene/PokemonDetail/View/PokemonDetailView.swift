//
//  PokemonDetailView.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 25/07/21.
//

import SwiftUI

struct PokemonDetailView: View {
    
    @ObservedObject var remoteImageURL: RemoteImageURL
    @Binding var selectedItem: PokemonsListItemViewModel
    @Binding var show: Bool
    
    var animation: Namespace.ID
    
    @State var loadContent = false
    @State var selectedColor: Color = Color("p1")
    
    var body: some View {
        
        // optimisation for smaller size iPhone
        ScrollView(UIScreen.main.bounds.height < 750 ? .vertical : .init()) {
            VStack {
                HStack(spacing: 25) {
                    Button(action: {
                        withAnimation(.spring()) {
                            show.toggle()
                        }
                    }){
                        Image(systemName: "chevron.left")
                            .font(.title)
                            .foregroundColor(.black)
                    }
                    
                    Spacer()
                    
                    Button(action: {}) {
                        Image(systemName: "magnifyingglass")
                            .font(.title)
                            .foregroundColor(.black)
                    }
                    
                    Button(action: {}) {
                        Image(systemName: "bag")
                            .font(.title)
                            .foregroundColor(.black)
                    }
                }
                .padding()
                
                VStack(spacing: 10) {
                    Image(uiImage: UIImage(data: remoteImageURL.data.image!) ?? UIImage())
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        // since id is common
                        .matchedGeometryEffect(id: "image\(selectedItem.name)", in: animation)
                        .padding()
                    
                    Text(selectedItem.name)
                        .font(.title)
                        .fontWeight(.heavy)
                        .foregroundColor(.black)
                    
                    Text(selectedItem.name)
                        .foregroundColor(.gray)
                        .padding(.top, 4)
                    
                    HStack {
                        Text(selectedItem.name)
                            .fontWeight(.heavy)
                            .foregroundColor(.black)
                            .matchedGeometryEffect(id: "ratingp1", in: animation)
                        
                        Spacer()
                        
                        Button(action: {}) {
                            Image(systemName: "suit.heart")
                                .font(.title2)
                                .foregroundColor(.black)
                        }
                        .matchedGeometryEffect(id: "heartp1", in: animation)
                    }
                    .padding()
                }
                .padding(.top, 35)
                .background(Color("p1")
                                .clipShape(CustomShape())
                                .matchedGeometryEffect(id: "color\(selectedItem.name)", in: animation))
                .cornerRadius(15)
                .padding()
                
                // delay loading the content for smooth animation
                VStack {
                    VStack(alignment: .leading, spacing: 8) {
                        Text("Exclusive Offer")
                            .fontWeight(.heavy)
                            .foregroundColor(.black)
                        
                        HStack {
                            Text("Frame + Lens for $35")
                                .foregroundColor(.gray)
                            
                            Text("50% off!")
                                .foregroundColor(.red)
                        }
                    }
                    .padding()
                    .frame(width: UIScreen.main.bounds.width - 30, alignment: .leading)
                    .background(Color("p3"))
                    .cornerRadius(15)
                    
                    VStack(alignment: .leading, spacing: 10) {
                        Text("Color")
                            .fontWeight(.heavy)
                            .foregroundColor(.black)
                        
                        HStack(spacing: 15) {
                            ForEach(1...4, id: \.self) { cardPokemon in
                                ZStack {
                                    Color("p\(cardPokemon)")
                                        .clipShape(Circle())
                                        .frame(width: 45, height: 45)
                                        .onTapGesture {
                                            withAnimation {
                                                selectedColor = Color("p\(cardPokemon)")
                                            }
                                        }
                                    // checkmark for selected one
                                    if selectedColor == Color("p\(cardPokemon)") {
                                        Image(systemName: "checkmark")
                                            .foregroundColor(.black)
                                    }
                                }
                            }
                            Spacer(minLength: 0)
                        }
                    }
                    .padding()
                    Spacer(minLength: 15)
                    Button(action: {}) {
                        Text("TRY FRAME IN 3D")
                            .fontWeight(.bold)
                            .foregroundColor(.black)
                            .padding(.vertical)
                            .frame(width: UIScreen.main.bounds.width - 100)
                            .background(
                                RoundedRectangle(cornerRadius: 15)
                                    .stroke(Color.black, lineWidth: 1)
                            )
                    }
                    Button(action: {}) {
                        Text("ADD TO CART")
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .padding(.vertical)
                            .frame(width: UIScreen.main.bounds.width - 100)
                            .background(Color.black)
                            .cornerRadius(15)
                    }
                    .padding(.vertical)
                }
                .padding([.horizontal, .bottom])
                .frame(height: loadContent ? nil : 0)
                .opacity(loadContent ? 1 : 0)
                // for smooth transition
                
                Spacer(minLength: 0)
            }
        }
        .onAppear {
            withAnimation(Animation.spring().delay(0.45)) {
                loadContent.toggle()
            }
        }
    }
}
