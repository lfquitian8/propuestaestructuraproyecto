//
//  PokemonsListView.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import SwiftUI
import Combine

struct PokemonsListView: View {
    
    @ObservedObject var viewModel: PokemonsListViewModel
    
    @State var selected = tabs[0]
    @Namespace var animation
    
    @State var show = false
    @State var selectedItem: PokemonsListItemViewModel = PokemonsListItemViewModel.init(id: "1", name: "prueba", url: "prueba",image: Data())
    
    
    
    var body: some View {
        ZStack {
            VStack {
                HStack {
                    Button(action: {}) {
                        Image(systemName: "line.horizontal.3.decrease")
                            .font(.system(size: 25, weight: .heavy))
                            .foregroundColor(.black).onTapGesture {
                                withAnimation(.interactiveSpring()) {
                                }
                            }
                    }
                    Spacer(minLength: 0)
                    Button(action: {}) {
                        Image("profile")
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 45, height: 45)
                            .cornerRadius(15)
                    }
                }
                .padding(.vertical, 10)
                .padding(.horizontal)
                
                ScrollView {
                        LazyVGrid(columns: Array(repeating: GridItem(.flexible(), spacing: 20), count: 2), spacing: 25) {
                            ForEach(ListPokemons.shared.pokemonsItems) { pokemon in
                                CardView(viewModel: viewModel, remoteImageURL: RemoteImageURL(pokemon: pokemon), pokemon: pokemon, animation: animation)
                                    .onTapGesture {
                                        withAnimation(.spring()) {
                                            selectedItem = pokemon
                                            show.toggle()
                                        }
                                    }
                            }
                    }.padding()
                    
                }
                VStack {
                    HStack(spacing: 0) {
                        ForEach(tabs, id: \.self) { tab in
                            // Tab Button
                            TabButton(selectedItem:$selectedItem, title: tab, selected: $selected, animation: animation ).environmentObject(viewModel)
                            
                            if tabs.last != tab {
                                Spacer(minLength: 0)
                            }
                        }
                    }
                    .padding()
                    .padding(.top, 5)
                }
                Spacer(minLength: 0)
            }
            .opacity(show ? 0 : 1)
            if show {
                PokemonDetailView(remoteImageURL: RemoteImageURL(pokemon: selectedItem), selectedItem: $selectedItem, show: $show, animation: animation)
            }
        }
        .background(Color.white.ignoresSafeArea())
    }

}

var tabs = ["1 Seson", "2 Seson", "3 Seson", "4 Seson"]
