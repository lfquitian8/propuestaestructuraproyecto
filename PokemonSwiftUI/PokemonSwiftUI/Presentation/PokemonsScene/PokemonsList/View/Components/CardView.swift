//
//  CardView.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import SwiftUI
import Combine

struct CardView: View {
    
    @ObservedObject var viewModel: PokemonsListViewModel
    @ObservedObject var remoteImageURL: RemoteImageURL
    
    var pokemon: PokemonsListItemViewModel
    var animation: Namespace.ID
    

    var body: some View {
        VStack {
            HStack {
                Spacer(minLength: 0)
                
                Text(pokemon.name)
                    .fontWeight(.heavy)
                    .foregroundColor(.black)
                    .padding(.vertical, 8)
                    .padding(.horizontal, 10)
                    .background(Color.white.opacity(0.5))
                    .cornerRadius(10)
            }
            
            Image(uiImage: UIImage(data: remoteImageURL.data.image!) ?? UIImage())
                .resizable()
                .aspectRatio(contentMode: .fit)
                .matchedGeometryEffect(id: "image\(pokemon.name)", in: animation)
                .padding(.top, 30)
                .padding(.bottom)
                .padding(.horizontal, 10)
            
            Text(pokemon.name)
                .fontWeight(.bold)
                .foregroundColor(.black)
            
            Text(pokemon.name)
                .font(.caption)
                .foregroundColor(.gray)
            
            // Using Matched Geometry Effect for Hero Animation
            HStack {
                Button(action: {}) {
                    Image(systemName: "suit.heart")
                        .font(.title2)
                        .foregroundColor(.black)
                }
                .matchedGeometryEffect(id: "heartp1", in: animation)
                
                Spacer(minLength: 0)
                
                Text(pokemon.name)
                    .fontWeight(.heavy)
                    .foregroundColor(.black)
                    .matchedGeometryEffect(id: "ratingp1", in: animation)
            }
            .padding()
        }
        // giving hero effect for color also
        .background(Color("p1")
                        .matchedGeometryEffect(id: "color\(pokemon.name)", in: animation))
        .cornerRadius(15)
    }
}
