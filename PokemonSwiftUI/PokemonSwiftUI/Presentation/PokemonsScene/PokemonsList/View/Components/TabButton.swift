//
//  TabButton.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import SwiftUI

struct TabButton: View {
    
    
    @EnvironmentObject var viewModel: PokemonsListViewModel
    
    @Binding var selectedItem: PokemonsListItemViewModel
    
    var title: String
    @Binding var selected: String
    
    var animation: Namespace.ID
    
    var body: some View {
        Button(action: {
            withAnimation(.spring()) {
                selected = title
            }
            selectedItem = PokemonsListItemViewModel.init(id: "1", name: "prueba", url: "prueba",image: Data())
            viewModel.prints(string: selected)
        }) {
            Text(title)
                .font(.system(size: 13))
                .fontWeight(.bold)
                .foregroundColor(selected == title ? .white : .black)
                .padding(.vertical, 10)
                .padding(.horizontal, selected == title ? 15 : 0)
                .background(
                    // For Slide Effect Animation
                    ZStack {
                        if selected == title {
                            Color.black
                                .clipShape(Capsule())
                                .matchedGeometryEffect(id: "Tab", in: animation)
                        }
                    }
                )
        }
    }
}

