//
//  PokemonsListViewModel.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

struct PokemonsListViewModelActions {
    let showPokemonDetails: (Pokemon) -> Void
}

enum PokemonsListViewModelLoading {
    case fullScreen
    case nextPage
}

final class PokemonsListViewModel: ObservableObject {

    private let searchPokemonsUseCase: ListPokemonsUseCase
    private let actions: PokemonsListViewModelActions?
    var currentPage: Int = 0
    var totalPageCount: Int = 1
    var hasMorePages: Bool { currentPage < totalPageCount }
    var nextPage: Int { hasMorePages ? currentPage + 1 : currentPage }

    private var pages: [PokemonsPage] = []
    private var pokemonsLoadTask: Cancellable? { willSet { pokemonsLoadTask?.cancel() } }
    
    // MARK: - Init

    init(searchPokemonsUseCase: ListPokemonsUseCase,
         actions: PokemonsListViewModelActions? = nil) {
        self.searchPokemonsUseCase = searchPokemonsUseCase
        self.actions = actions
        self.didSearch(query: "")
    }

    // MARK: - Private

    private func appendPage(_ pokemonsPage: PokemonsPage) {
        currentPage = pokemonsPage.page
        pages = pages
            .filter { $0.page != pokemonsPage.page }
            + [pokemonsPage]
        ListPokemons.shared.pokemonsItems.removeAll()
        ListPokemons.shared.pokemonsItems = pages.pokemons.map(PokemonsListItemViewModel.init)
    }

    private func resetPages() {
        currentPage = 0
        totalPageCount = 1
        pages.removeAll()
        ListPokemons.shared.pokemonsItems.removeAll()
    }

    private func load(pokemonQuery: PokemonQuery, loading: PokemonsListViewModelLoading) {
        pokemonsLoadTask = searchPokemonsUseCase.execute(
            requestValue: .init(query: pokemonQuery, page: nextPage),
            cached: appendPage,
            completion: { result in
                switch result {
                case .success(let page):
                    self.appendPage(page)
                case .failure(let error):
                    self.handle(error: error)
                }
               
        })
    }

    private func handle(error: Error) {
//        self.error.value = error.isInternetConnectionError ?
//            NSLocalizedString("No internet connection", comment: "") :
//            NSLocalizedString("Failed loading pokemons", comment: "")
    }

    private func update(pokemonQuery: PokemonQuery) {
        resetPages()
        load(pokemonQuery: pokemonQuery, loading: .fullScreen)
    }
}

// MARK: - INPUT. View event methods

extension PokemonsListViewModel {

    func didSearch(query: String) {
        update(pokemonQuery: PokemonQuery(query: query))
    }
    func prints(string: String) {
        print(string)
    }

}

// MARK: - Private

private extension Array where Element == PokemonsPage {
    var pokemons: [Pokemon] { flatMap { $0.pokemons } }
}
