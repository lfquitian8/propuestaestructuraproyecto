//
//  PokemonsListItemViewModel.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//
// **Note**: This item view model is to display data and does not contain any domain model to prevent views accessing it

import Foundation

struct PokemonsListItemViewModel: Equatable,Identifiable {
    let id: String?
    let name: String
    let url: String
    var image: Data?
}

extension PokemonsListItemViewModel {

    init(pokemon: Pokemon) {
        self.id = UUID().uuidString
        self.name = pokemon.name ?? ""
        self.url = pokemon.url ?? ""
        self.image = Data()
    }
}

final class ListPokemons {

    static let shared = ListPokemons()
    
    var pokemonsItems: [PokemonsListItemViewModel]
    
    init(){
        pokemonsItems = [PokemonsListItemViewModel.init(id: "", name: "prueba", url: "prueba", image: Data())]
    }

}
