//
//  View+Extension.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 25/07/21.
//

import SwiftUI

extension View {
    func eraseToAnyView() -> AnyView { AnyView(self) }
}
