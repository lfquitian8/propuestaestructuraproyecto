//
//  DefaultPokemonsRepository.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//
// **Note**: DTOs structs are mapped into Domains here, and Repository protocols does not contain DTOs

import Foundation

final class DefaultPokemonsRepository {

    private let dataTransferService: DataTransferService
    private let cache: PokemonsResponseStorage

    init(dataTransferService: DataTransferService, cache: PokemonsResponseStorage) {
        self.dataTransferService = dataTransferService
        self.cache = cache
    }
}

extension DefaultPokemonsRepository: PokemonsRepository {

    public func fetchPokemonsList(query: PokemonQuery, page: Int,
                                cached: @escaping (PokemonsPage) -> Void,
                                completion: @escaping (Result<PokemonsPage, Error>) -> Void) -> Cancellable? {

        let requestDTO = PokemonsRequestDTO(query: query.query, page: page)
        let task = RepositoryTask()

        cache.getResponse(for: requestDTO) { result in

            if case let .success(responseDTO?) = result {
                cached(responseDTO.toDomain())
            }
            guard !task.isCancelled else { return }

            let endpoint = APIEndpoints.getPokemons(with: requestDTO)
            task.networkTask = self.dataTransferService.request(with: endpoint) { result in
                switch result {
                case .success(let responseDTO):
                    self.cache.save(response: responseDTO, for: requestDTO)
                    completion(.success(responseDTO.toDomain()))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
        return task
    }
}
