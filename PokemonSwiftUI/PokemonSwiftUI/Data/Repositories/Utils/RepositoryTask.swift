//
//  RepositoryTask.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

class RepositoryTask: Cancellable {
    var networkTask: NetworkCancellable?
    var isCancelled: Bool = false
    
    func cancel() {
        networkTask?.cancel()
        isCancelled = true
    }
}
