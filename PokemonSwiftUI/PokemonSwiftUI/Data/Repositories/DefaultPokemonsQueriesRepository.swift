//
//  DefaultPokemonsQueriesRepository.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

final class DefaultPokemonsQueriesRepository {
    
    private let dataTransferService: DataTransferService
    private var pokemonsQueriesPersistentStorage: PokemonsQueriesStorage
    
    init(dataTransferService: DataTransferService,
         pokemonsQueriesPersistentStorage: PokemonsQueriesStorage) {
        self.dataTransferService = dataTransferService
        self.pokemonsQueriesPersistentStorage = pokemonsQueriesPersistentStorage
    }
}

extension DefaultPokemonsQueriesRepository: PokemonsQueriesRepository {
    
    func fetchRecentsQueries(maxCount: Int, completion: @escaping (Result<[PokemonQuery], Error>) -> Void) {
        return pokemonsQueriesPersistentStorage.fetchRecentsQueries(maxCount: maxCount, completion: completion)
    }
    
    func saveRecentQuery(query: PokemonQuery, completion: @escaping (Result<PokemonQuery, Error>) -> Void) {
        pokemonsQueriesPersistentStorage.saveRecentQuery(query: query, completion: completion)
    }
}

