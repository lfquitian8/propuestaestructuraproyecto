//
//  PokemonsRequestDTO.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation

struct PokemonsRequestDTO: Encodable {
    let query: String
    let page: Int
}
