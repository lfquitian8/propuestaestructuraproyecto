//
//  PokemonsResponseDTO+Mapping.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation

// MARK: - Data Transfer Object

struct PokemonsResponseDTO: Decodable {
    private enum CodingKeys: String, CodingKey {
        case page = "count"
        case pokemons = "results"
    }
    let page: Int
    let pokemons: [PokemonDTO]
    
}

extension PokemonsResponseDTO {
    struct PokemonDTO: Decodable {
        private enum CodingKeys: String, CodingKey {
            case name
            case url
        }
        let name: String?
        let url: String?
    }
}

// MARK: - Mappings to Domain

extension PokemonsResponseDTO {
    func toDomain() -> PokemonsPage {
        return .init(page: page,
                     pokemons: pokemons.map { $0.toDomain() })
    }
}

extension PokemonsResponseDTO.PokemonDTO {
    func toDomain() -> Pokemon {
        
        return .init(name: name,
                     url: url)
    }
}




// MARK: - Private

private let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    return formatter
}()
