//
//  APIEndpoints.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

struct APIEndpoints {
    
    static func getPokemons(with pokemonsRequestDTO: PokemonsRequestDTO) -> Endpoint<PokemonsResponseDTO> {

        return Endpoint(path: "pokemon/",
                        method: .get,
                        queryParametersEncodable: pokemonsRequestDTO)
    }

   
}
