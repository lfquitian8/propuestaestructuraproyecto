//
//  PokemonQueriesStorage.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation

protocol PokemonsQueriesStorage {
    func fetchRecentsQueries(maxCount: Int, completion: @escaping (Result<[PokemonQuery], Error>) -> Void)
    func saveRecentQuery(query: PokemonQuery, completion: @escaping (Result<PokemonQuery, Error>) -> Void)
}
