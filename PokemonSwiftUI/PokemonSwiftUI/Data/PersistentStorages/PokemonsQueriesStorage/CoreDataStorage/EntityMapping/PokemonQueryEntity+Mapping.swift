//
//  PokemonQueryEntity+Mapping.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation
import CoreData

extension PokemonQueryEntity {
    convenience init(pokemonQuery: PokemonQuery, insertInto context: NSManagedObjectContext) {
        self.init(context: context)
        query = pokemonQuery.query
        createdAt = Date()
    }
}

extension PokemonQueryEntity {
    func toDomain() -> PokemonQuery {
        return .init(query: query ?? "")
    }
}
