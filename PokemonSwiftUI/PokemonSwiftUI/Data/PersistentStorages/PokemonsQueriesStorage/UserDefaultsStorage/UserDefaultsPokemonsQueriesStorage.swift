//
//  UserDefaultsPokemonsQueriesStorage.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

final class UserDefaultsPokemonsQueriesStorage {
    private let maxStorageLimit: Int
    private let recentsPokemonsQueriesKey = "recentsPokemonsQueries"
    private var userDefaults: UserDefaults
    
    init(maxStorageLimit: Int, userDefaults: UserDefaults = UserDefaults.standard) {
        self.maxStorageLimit = maxStorageLimit
        self.userDefaults = userDefaults
    }

    private func fetchPokemonsQuries() -> [PokemonQuery] {
        if let queriesData = userDefaults.object(forKey: recentsPokemonsQueriesKey) as? Data {
            if let pokemonQueryList = try? JSONDecoder().decode(PokemonQueriesListUDS.self, from: queriesData) {
                return pokemonQueryList.list.map { $0.toDomain() }
            }
        }
        return []
    }

    private func persist(pokemonsQuries: [PokemonQuery]) {
        let encoder = JSONEncoder()
        let pokemonQueryUDSs = pokemonsQuries.map(PokemonQueryUDS.init)
        if let encoded = try? encoder.encode(PokemonQueriesListUDS(list: pokemonQueryUDSs)) {
            userDefaults.set(encoded, forKey: recentsPokemonsQueriesKey)
        }
    }
}

extension UserDefaultsPokemonsQueriesStorage: PokemonsQueriesStorage {

    func fetchRecentsQueries(maxCount: Int, completion: @escaping (Result<[PokemonQuery], Error>) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }

            var queries = self.fetchPokemonsQuries()
            queries = queries.count < self.maxStorageLimit ? queries : Array(queries[0..<maxCount])
            completion(.success(queries))
        }
    }

    func saveRecentQuery(query: PokemonQuery, completion: @escaping (Result<PokemonQuery, Error>) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let self = self else { return }

            var queries = self.fetchPokemonsQuries()
            self.cleanUpQueries(for: query, in: &queries)
            queries.insert(query, at: 0)
            self.persist(pokemonsQuries: queries)

            completion(.success(query))
        }
    }
}


// MARK: - Private
extension UserDefaultsPokemonsQueriesStorage {

    private func cleanUpQueries(for query: PokemonQuery, in queries: inout [PokemonQuery]) {
        removeDuplicates(for: query, in: &queries)
        removeQueries(limit: maxStorageLimit - 1, in: &queries)
    }

    private func removeDuplicates(for query: PokemonQuery, in queries: inout [PokemonQuery]) {
        queries = queries.filter { $0 != query }
    }

    private func removeQueries(limit: Int, in queries: inout [PokemonQuery]) {
        queries = queries.count <= limit ? queries : Array(queries[0..<limit])
    }
}
