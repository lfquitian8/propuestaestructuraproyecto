//
//  PokemonQueryUDS+Mapping.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

struct PokemonQueriesListUDS: Codable {
    var list: [PokemonQueryUDS]
}

struct PokemonQueryUDS: Codable {
    let query: String
}

extension PokemonQueryUDS {
    init(pokemonQuery: PokemonQuery) {
        query = pokemonQuery.query
    }
}

extension PokemonQueryUDS {
    func toDomain() -> PokemonQuery {
        return .init(query: query)
    }
}
