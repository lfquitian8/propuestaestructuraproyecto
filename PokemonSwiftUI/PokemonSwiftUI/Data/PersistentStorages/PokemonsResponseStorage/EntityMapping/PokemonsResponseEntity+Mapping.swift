//
//  PokemonsResponseEntity+Mapping.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation
import CoreData

extension PokemonsResponseEntity {
    func toDTO() -> PokemonsResponseDTO {
        return .init(page: Int(page),
                     pokemons: pokemons?.allObjects.map { ($0 as! PokemonResponseEntity).toDTO() } ?? [])
    }
}

extension PokemonResponseEntity {
    func toDTO() -> PokemonsResponseDTO.PokemonDTO {
        return .init(name: name,
                     url: url)
    }
}

extension PokemonsRequestDTO {
    func toEntity(in context: NSManagedObjectContext) -> PokemonsRequestEntity {
        let entity: PokemonsRequestEntity = .init(context: context)
        entity.query = query
        entity.page = Int32(page)
        return entity
    }
}

extension PokemonsResponseDTO {
    func toEntity(in context: NSManagedObjectContext) -> PokemonsResponseEntity {
        let entity: PokemonsResponseEntity = .init(context: context)
        entity.page = Int32(page)
        pokemons.forEach {
            entity.addToPokemons($0.toEntity(in: context))
        }
        return entity
    }
}

extension PokemonsResponseDTO.PokemonDTO {
    func toEntity(in context: NSManagedObjectContext) -> PokemonResponseEntity {
        let entity: PokemonResponseEntity = .init(context: context)
        entity.name = name
        entity.url = url
        return entity
    }
}
