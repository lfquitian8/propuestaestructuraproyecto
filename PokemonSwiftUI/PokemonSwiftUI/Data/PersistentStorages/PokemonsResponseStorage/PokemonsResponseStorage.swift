//
//  PokemonsResponseStorage.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation

protocol PokemonsResponseStorage {
    func getResponse(for request: PokemonsRequestDTO, completion: @escaping (Result<PokemonsResponseDTO?, CoreDataStorageError>) -> Void)
    func save(response: PokemonsResponseDTO, for requestDto: PokemonsRequestDTO)
}
