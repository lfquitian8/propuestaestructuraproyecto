//
//  RemoteImageURL.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 25/07/21.
//

import Foundation

class RemoteImageURL: ObservableObject {
    @Published var data: PokemonsListItemViewModel = PokemonsListItemViewModel.init(id: "", name: "prueba", url: "prueba", image: Data())
    
    init(pokemon: PokemonsListItemViewModel) {
        guard let imageBaseURL = Bundle.main.object(forInfoDictionaryKey: "ImageBaseURL") as? String else {
            fatalError("ApiBaseURL must not be empty in plist")
        }
        let idPokemon = pokemon.url.components(separatedBy: "/")
        guard let url = URL(string: "\(imageBaseURL)\(idPokemon[6]).png") else { return }

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }

            DispatchQueue.main.async {
                if let row = ListPokemons.shared.pokemonsItems.firstIndex(where: {$0.name == pokemon.name}) {
                    ListPokemons.shared.pokemonsItems[row].image = data
                    self.data = ListPokemons.shared.pokemonsItems[row]
                }
                
            }

            }.resume()
    }
}
