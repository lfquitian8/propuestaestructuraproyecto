//
//  ServiceConfigurations.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 22/07/21.
//

import Foundation

final class ServiceConfiguration {
    
    lazy var baseURL: String = {
        guard let apiBaseURL = Bundle.main.object(forInfoDictionaryKey: "BaseURL") as? String else {
            fatalError("ApiBaseURL must not be empty in plist")
        }
        return apiBaseURL
    }()
}
