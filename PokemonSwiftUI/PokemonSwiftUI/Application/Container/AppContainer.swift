//
//  APPContainer.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 22/07/21.
//

import Foundation

final class AppContainer {
    lazy var appConfiguration = ServiceConfiguration()
    // MARK: - Network
    lazy var apiDataTransferSer: DataTransferService = {
        let config = ApiDataNetworkConfig(baseURL: URL(string: appConfiguration.baseURL)!,
                                          queryParameters: ["language": NSLocale.preferredLanguages.first ?? "en"])
        
        let apiDataNetwork = DefaultNetworkService(config: config)
        return DefaultDataTransferService(with: apiDataNetwork)
    }()
    
    // MARK: - Pokemon of scenes
    func makePokemonsSceneContainer()-> PokemonsSceneContainer {
        let dependencies = Dependencies(apiDataTransferService: apiDataTransferSer)
        return PokemonsSceneContainer(dependencies: dependencies)
    }
}
