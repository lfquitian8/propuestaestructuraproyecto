//
//  PokemonsSceneContainer.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import UIKit
import SwiftUI

struct Dependencies {
    let apiDataTransferService: DataTransferService
}

final class PokemonsSceneContainer {
    private let dependencies: Dependencies

    // MARK: - Persistent Storage
    lazy var pokemonsQueriesStorage: PokemonsQueriesStorage = CoreDataPokemonsQueriesStorage(maxStorageLimit: 10)
    lazy var pokemonsResponseCache: PokemonsResponseStorage = CoreDataPokemonsResponseStorage()

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    // MARK: - Use Cases
    func makeListPokemonsUseCase() -> ListPokemonsUseCase {
        return DefaultSearchPokemonsUseCase(pokemonsRepository: makePokemonsRepository(),
                                          pokemonsQueriesRepository: makePokemonsQueriesRepository())
    }
    func makeFetchRecentPokemonQueriesUseCase(requestValue: FetchRecentPokemonQueriesUseCase.RequestValue,
                                            completion: @escaping (FetchRecentPokemonQueriesUseCase.ResultValue) -> Void) -> UseCase {
        return FetchRecentPokemonQueriesUseCase(requestValue: requestValue,
                                              completion: completion,
                                              pokemonsQueriesRepository: makePokemonsQueriesRepository()
        )
    }
    // MARK: - Repositories
    func makePokemonsRepository() -> PokemonsRepository {
        return DefaultPokemonsRepository(dataTransferService: dependencies.apiDataTransferService, cache: pokemonsResponseCache)
    }
    func makePokemonsQueriesRepository() -> PokemonsQueriesRepository {
        return DefaultPokemonsQueriesRepository(dataTransferService: dependencies.apiDataTransferService,
                                              pokemonsQueriesPersistentStorage: pokemonsQueriesStorage)
    }

    // MARK: - Pokemons List
    func makePokemonsListView(actions: PokemonsListViewModelActions) -> PokemonsListView {
        return PokemonsListView(viewModel: makePokemonsListViewModel(actions: actions))
    }

    func makePokemonsListViewModel(actions: PokemonsListViewModelActions) -> PokemonsListViewModel {
        let viewModel = PokemonsListViewModel(searchPokemonsUseCase: makeListPokemonsUseCase(),
                                          actions: actions)
        return viewModel
    }

    // MARK: - Flow Coordinators
    func makePokemonsListFlowCoordinator() -> PokemonsListFlowCoordinator {
        return PokemonsListFlowCoordinator(dependencies: self)
    }
}
//
extension PokemonsSceneContainer: PokemonsListFlowCoordinatorDependencies {}
