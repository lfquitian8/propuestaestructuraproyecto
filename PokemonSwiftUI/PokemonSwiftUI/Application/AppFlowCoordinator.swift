//
//  AppFlowCoordinator.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import SwiftUI

final class AppFlowCoordinator {

    private let appContainer: AppContainer
    
    init(appContainer: AppContainer) {
        self.appContainer = appContainer
    }

    func start() -> PokemonsListView {
        // In App Flow we can check if user needs to login, if yes we would run login flow
        let moviesSceneContainer = appContainer.makePokemonsSceneContainer()
        let flow = moviesSceneContainer.makePokemonsListFlowCoordinator()
        return flow.start()
    }
}
