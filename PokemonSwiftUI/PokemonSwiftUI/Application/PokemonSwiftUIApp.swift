//
//  PokemonSwiftUIApp.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 22/07/21.
//

import SwiftUI

@main
struct PokemonSwiftUIApp: App {
    
    let appContainer = AppContainer()
    var appFlowCoordinator: AppFlowCoordinator?
    let view: PokemonsListView
    
    init() {
        CoreDataStorage.shared.saveContext()
        appFlowCoordinator = AppFlowCoordinator(appContainer: appContainer)
        view = (appFlowCoordinator?.start())!
    }
    
    var body: some Scene {
        WindowGroup {
            view
        }
    }
}
