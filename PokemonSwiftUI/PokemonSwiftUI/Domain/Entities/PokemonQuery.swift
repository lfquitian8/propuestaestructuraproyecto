//
//  PokemonQuery.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation

struct PokemonQuery: Equatable {
    let query: String
}
