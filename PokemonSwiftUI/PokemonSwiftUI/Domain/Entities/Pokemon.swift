//
//  Pokemon.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation

struct Pokemon: Equatable {

    let name: String?
    let url: String?
}

struct PokemonsPage: Equatable {
    let page: Int
    let pokemons: [Pokemon]
}
