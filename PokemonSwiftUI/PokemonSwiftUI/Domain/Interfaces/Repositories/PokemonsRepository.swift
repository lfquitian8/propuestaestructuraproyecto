//
//  PokemonsRepository.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

protocol PokemonsRepository {
    @discardableResult
    func fetchPokemonsList(query: PokemonQuery, page: Int,
                         cached: @escaping (PokemonsPage) -> Void,
                         completion: @escaping (Result<PokemonsPage, Error>) -> Void) -> Cancellable?
}
