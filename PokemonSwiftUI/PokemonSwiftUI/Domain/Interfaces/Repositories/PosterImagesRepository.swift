//
//  PosterImagesRepository.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

protocol PosterImagesRepository {
    func fetchImage(with imagePath: String, completion: @escaping (Result<Data, Error>) -> Void) -> Cancellable?
}
