//
//  PokemonsQueriesRepository.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

protocol PokemonsQueriesRepository {
    func fetchRecentsQueries(maxCount: Int, completion: @escaping (Result<[PokemonQuery], Error>) -> Void)
    func saveRecentQuery(query: PokemonQuery, completion: @escaping (Result<PokemonQuery, Error>) -> Void)
}

