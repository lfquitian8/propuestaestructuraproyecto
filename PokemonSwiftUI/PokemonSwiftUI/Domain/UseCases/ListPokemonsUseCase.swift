//
//  SearchPokemonsUseCase.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

protocol ListPokemonsUseCase {
    func execute(requestValue: SearchPokemonsUseCaseRequestValue,
                 cached: @escaping (PokemonsPage) -> Void,
                 completion: @escaping (Result<PokemonsPage, Error>) -> Void) -> Cancellable?
}

final class DefaultSearchPokemonsUseCase: ListPokemonsUseCase {

    private let pokemonsRepository: PokemonsRepository
    private let pokemonsQueriesRepository: PokemonsQueriesRepository

    init(pokemonsRepository: PokemonsRepository,
         pokemonsQueriesRepository: PokemonsQueriesRepository) {

        self.pokemonsRepository = pokemonsRepository
        self.pokemonsQueriesRepository = pokemonsQueriesRepository
    }

    func execute(requestValue: SearchPokemonsUseCaseRequestValue,
                 cached: @escaping (PokemonsPage) -> Void,
                 completion: @escaping (Result<PokemonsPage, Error>) -> Void) -> Cancellable? {

        return pokemonsRepository.fetchPokemonsList(query: requestValue.query,
                                                page: requestValue.page,
                                                cached: cached) { result in

            if case .success = result {
                self.pokemonsQueriesRepository.saveRecentQuery(query: requestValue.query) { _ in }
            }

            completion(result)
        }
    }
}

struct SearchPokemonsUseCaseRequestValue {
    let query: PokemonQuery
    let page: Int
}
