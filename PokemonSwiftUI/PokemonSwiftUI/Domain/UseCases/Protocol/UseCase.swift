//
//  UseCase.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

public protocol UseCase {
    @discardableResult
    func start() -> Cancellable?
}
