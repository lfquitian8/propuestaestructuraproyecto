//
//  FetchRecentPokemonQueriesUseCase.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 24/07/21.
//

import Foundation

// This is another option to create Use Case using more generic way
final class FetchRecentPokemonQueriesUseCase: UseCase {

    struct RequestValue {
        let maxCount: Int
    }
    typealias ResultValue = (Result<[PokemonQuery], Error>)

    private let requestValue: RequestValue
    private let completion: (ResultValue) -> Void
    private let pokemonsQueriesRepository: PokemonsQueriesRepository

    init(requestValue: RequestValue,
         completion: @escaping (ResultValue) -> Void,
         pokemonsQueriesRepository: PokemonsQueriesRepository) {

        self.requestValue = requestValue
        self.completion = completion
        self.pokemonsQueriesRepository = pokemonsQueriesRepository
    }
    
    func start() -> Cancellable? {

        pokemonsQueriesRepository.fetchRecentsQueries(maxCount: requestValue.maxCount, completion: completion)
        return nil
    }
}

