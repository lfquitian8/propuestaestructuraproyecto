//
//  BodyEncoding.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation

public enum BodyEncoding {
    case jsonSerializationData
    case stringEncodingAscii
}
