//
//  RequestGenerationError.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation

enum RequestGenerationError: Error {
    case components
}
