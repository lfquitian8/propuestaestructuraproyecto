//
//  HTTPMethodType.swift
//  PokemonSwiftUI
//
//  Created by luis quitan on 23/07/21.
//

import Foundation

public enum HTTPMethodType: String {
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}
