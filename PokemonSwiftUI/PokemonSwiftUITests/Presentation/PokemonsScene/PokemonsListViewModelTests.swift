////
////  PokemonsListViewModelTests.swift
////  PokemonSwiftUITests
////
////  Created by luis quitan on 23/08/21.
////
//
//
//import XCTest
//
//class PokemonsListViewModelTests: XCTestCase {
//    
//    private enum SearchPokemonsUseCaseError: Error {
//        case someError
//    }
//    
//    let pokemonsPages: [PokemonsPage] = {
//        let page1 = PokemonsPage(page: 1, pokemons: [
//            Pokemon.stub(name: "bulbasaur", url: "https://pokeapi.co/api/v2/pokemon/1/"),
//            Pokemon.stub(name: "ivysaur", url: "https://pokeapi.co/api/v2/pokemon/2/")])
//        let page2 = PokemonsPage(page: 2, pokemons: [
//            Pokemon.stub(name: "venusaur", url: "https://pokeapi.co/api/v2/pokemon/3/")])
//        return [page1, page2]
//    }()
//    
//    class SearchPokemonsUseCaseMock: ListPokemonsUseCase {
//        var expectation: XCTestExpectation?
//        var error: Error?
//        var page = PokemonsPage(page: 0, pokemons: [])
//        
//        func execute(requestValue: SearchPokemonsUseCaseRequestValue,
//                     cached: @escaping (PokemonsPage) -> Void,
//                     completion: @escaping (Result<PokemonsPage, Error>) -> Void) -> Cancellable? {
//            if let error = error {
//                completion(.failure(error))
//            } else {
//                completion(.success(page))
//            }
//            expectation?.fulfill()
//            return nil
//        }
//    }
//    
//    func test_whenSearchPokemonsUseCaseRetrievesFirstPage_thenViewModelContainsOnlyFirstPage() {
//        // given
//        let searchPokemonsUseCaseMock = SearchPokemonsUseCaseMock()
//        searchPokemonsUseCaseMock.expectation = self.expectation(description: "contains only first page")
//        searchPokemonsUseCaseMock.page = PokemonsPage(page: 1, pokemons: pokemonsPages[0].pokemons)
//        let viewModel = PokemonsListViewModel(searchPokemonsUseCase: searchPokemonsUseCaseMock)
//        // when
//        viewModel.didSearch(query: "query")
//        
//        // then
//        waitForExpectations(timeout: 5, handler: nil)
//        XCTAssertEqual(viewModel.currentPage, 1)
//    }
//    
//    func test_whenSearchPokemonsUseCaseRetrievesFirstAndSecondPage_thenViewModelContainsTwoPages() {
//        // given
//        let searchPokemonsUseCaseMock = SearchPokemonsUseCaseMock()
//        searchPokemonsUseCaseMock.expectation = self.expectation(description: "First page loaded")
//        searchPokemonsUseCaseMock.page = PokemonsPage(page: 1, pokemons: pokemonsPages[0].pokemons)
//        let viewModel = PokemonsListViewModel(searchPokemonsUseCase: searchPokemonsUseCaseMock)
//        // when
//        viewModel.didSearch(query: "query")
//        waitForExpectations(timeout: 5, handler: nil)
//        
////        searchPokemonsUseCaseMock.expectation = self.expectation(description: "Second page loaded")
////        searchPokemonsUseCaseMock.page = PokemonsPage(page: 2, pokemons: pokemonsPages[1].pokemons)
////
////        viewModel.didLoadNextPage()
////
////        // then
////        waitForExpectations(timeout: 5, handler: nil)
////        XCTAssertEqual(viewModel.currentPage, 1)
//    }
//    
//    func test_whenSearchPokemonsUseCaseReturnsError_thenViewModelContainsError() {
//        // given
//        let searchPokemonsUseCaseMock = SearchPokemonsUseCaseMock()
//        searchPokemonsUseCaseMock.expectation = self.expectation(description: "contain errors")
//        searchPokemonsUseCaseMock.error = SearchPokemonsUseCaseError.someError
//        let viewModel = PokemonsListViewModel(searchPokemonsUseCase: searchPokemonsUseCaseMock as! ListPokemonsUseCase)
//        // when
//        viewModel.didSearch(query: "query")
//        
//        // then
//        waitForExpectations(timeout: 5, handler: nil)
////        XCTAssertNotNil(viewModel.error)
//    }
//    
//    func test_whenLastPage_thenHasNoPageIsTrue() {
//        // given
//        let searchPokemonsUseCaseMock = SearchPokemonsUseCaseMock()
//        searchPokemonsUseCaseMock.expectation = self.expectation(description: "First page loaded")
//        searchPokemonsUseCaseMock.page = PokemonsPage(page: 1, pokemons: pokemonsPages[0].pokemons)
//        let viewModel = PokemonsListViewModel(searchPokemonsUseCase: searchPokemonsUseCaseMock as! ListPokemonsUseCase)
//        // when
//        viewModel.didSearch(query: "query")
//        waitForExpectations(timeout: 5, handler: nil)
////
////        searchPokemonsUseCaseMock.expectation = self.expectation(description: "Second page loaded")
////        searchPokemonsUseCaseMock.page = PokemonsPage(page: 2, pokemons: pokemonsPages[1].pokemons)
//
//       
////
////        // then
////        waitForExpectations(timeout: 15, handler: nil)
////        XCTAssertEqual(viewModel.currentPage, 1)
////        XCTAssertFalse(viewModel.hasMorePages)
//    }
//}
