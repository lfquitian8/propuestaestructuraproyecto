//
//  ListPokemonsUseCaseTests.swift
//  PokemonSwiftUITests
//
//  Created by luis quitan on 23/08/21.
//

import XCTest

class ListPokemonsUseCaseTests: XCTestCase {
    
    static let pokemonsPages: [PokemonsPage] = {
        let page1 = PokemonsPage(page: 1, pokemons: [
            Pokemon.stub(name: "bulbasaur", url: "https://pokeapi.co/api/v2/pokemon/1/"),
            Pokemon.stub(name: "ivysaur", url: "https://pokeapi.co/api/v2/pokemon/2/")])
        let page2 = PokemonsPage(page: 2, pokemons: [
            Pokemon.stub(name: "venusaur", url: "https://pokeapi.co/api/v2/pokemon/3/")])
        return [page1, page2]
    }()
    
    enum PokemonsRepositorySuccessTestError: Error {
        case failedFetching
    }
    
    class PokemonsQueriesRepositoryMock: PokemonsQueriesRepository {
        var recentQueries: [PokemonQuery] = []
        
        func fetchRecentsQueries(maxCount: Int, completion: @escaping (Result<[PokemonQuery], Error>) -> Void) {
            completion(.success(recentQueries))
        }
        func saveRecentQuery(query: PokemonQuery, completion: @escaping (Result<PokemonQuery, Error>) -> Void) {
            recentQueries.append(query)
        }
    }
    
    struct PokemonsRepositoryMock: PokemonsRepository {
        var result: Result<PokemonsPage, Error>
        func fetchPokemonsList(query: PokemonQuery, page: Int, cached: @escaping (PokemonsPage) -> Void, completion: @escaping (Result<PokemonsPage, Error>) -> Void) -> Cancellable? {
            completion(result)
            return nil
        }
    }
    
    func testSearchPokemonsUseCase_whenSuccessfullyFetchesPokemonsForQuery_thenQueryIsSavedInRecentQueries() {
        // given
        let expectation = self.expectation(description: "Recent query saved")
        expectation.expectedFulfillmentCount = 2
        let pokemonsQueriesRepository = PokemonsQueriesRepositoryMock()
        let useCase = DefaultSearchPokemonsUseCase(pokemonsRepository: PokemonsRepositoryMock(result: .success(ListPokemonsUseCaseTests.pokemonsPages[0])),
                                                 pokemonsQueriesRepository: pokemonsQueriesRepository)

        // when
        let requestValue = SearchPokemonsUseCaseRequestValue(query: PokemonQuery(query: "title1"),
                                                           page: 0)
        _ = useCase.execute(requestValue: requestValue, cached: { _ in }) { _ in
            expectation.fulfill()
        }
        // then
        var recents = [PokemonQuery]()
        pokemonsQueriesRepository.fetchRecentsQueries(maxCount: 1) { result in
            recents = (try? result.get()) ?? []
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(recents.contains(PokemonQuery(query: "title1")))
    }
    
    func testSearchPokemonsUseCase_whenFailedFetchingPokemonsForQuery_thenQueryIsNotSavedInRecentQueries() {
        // given
        let expectation = self.expectation(description: "Recent query should not be saved")
        expectation.expectedFulfillmentCount = 2
        let pokemonsQueriesRepository = PokemonsQueriesRepositoryMock()
        let useCase = DefaultSearchPokemonsUseCase(pokemonsRepository: PokemonsRepositoryMock(result: .failure(PokemonsRepositorySuccessTestError.failedFetching)),
                                                 pokemonsQueriesRepository: pokemonsQueriesRepository)
        
        // when
        let requestValue = SearchPokemonsUseCaseRequestValue(query: PokemonQuery(query: "title1"),
                                                           page: 0)
        _ = useCase.execute(requestValue: requestValue, cached: { _ in }) { _ in
            expectation.fulfill()
        }
        // then
        var recents = [PokemonQuery]()
        pokemonsQueriesRepository.fetchRecentsQueries(maxCount: 1) { result in
            recents = (try? result.get()) ?? []
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(recents.isEmpty)
    }
}
