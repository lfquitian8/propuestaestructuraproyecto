//
//  Pokemon+Stub.swift
//  PokemonSwiftUITests
//
//  Created by luis quitan on 08/08/21.
//

import Foundation

extension Pokemon {
    static func stub(name: String = "bulbasaur",
                     url: String = "https://pokeapi.co/api/v2/pokemon/1/") -> Self {
        Pokemon(name: name,
                url: url)
    }
}

