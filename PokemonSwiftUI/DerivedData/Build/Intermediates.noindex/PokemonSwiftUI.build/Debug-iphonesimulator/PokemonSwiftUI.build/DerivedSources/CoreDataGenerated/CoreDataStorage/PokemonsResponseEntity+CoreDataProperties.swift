//
//  PokemonsResponseEntity+CoreDataProperties.swift
//  
//
//  Created by luis quitan on 3/08/21.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension PokemonsResponseEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PokemonsResponseEntity> {
        return NSFetchRequest<PokemonsResponseEntity>(entityName: "PokemonsResponseEntity")
    }

    @NSManaged public var page: Int32
    @NSManaged public var totalPages: Int32
    @NSManaged public var pokemons: NSSet?
    @NSManaged public var request: PokemonsRequestEntity?

}

// MARK: Generated accessors for pokemons
extension PokemonsResponseEntity {

    @objc(addPokemonsObject:)
    @NSManaged public func addToPokemons(_ value: PokemonResponseEntity)

    @objc(removePokemonsObject:)
    @NSManaged public func removeFromPokemons(_ value: PokemonResponseEntity)

    @objc(addPokemons:)
    @NSManaged public func addToPokemons(_ values: NSSet)

    @objc(removePokemons:)
    @NSManaged public func removeFromPokemons(_ values: NSSet)

}

extension PokemonsResponseEntity : Identifiable {

}
