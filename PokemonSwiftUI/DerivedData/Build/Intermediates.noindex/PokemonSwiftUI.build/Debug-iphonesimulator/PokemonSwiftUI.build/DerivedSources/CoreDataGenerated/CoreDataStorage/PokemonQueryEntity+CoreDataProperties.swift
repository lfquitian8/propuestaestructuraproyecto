//
//  PokemonQueryEntity+CoreDataProperties.swift
//  
//
//  Created by luis quitan on 3/08/21.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension PokemonQueryEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PokemonQueryEntity> {
        return NSFetchRequest<PokemonQueryEntity>(entityName: "PokemonQueryEntity")
    }

    @NSManaged public var createdAt: Date?
    @NSManaged public var query: String?

}

extension PokemonQueryEntity : Identifiable {

}
