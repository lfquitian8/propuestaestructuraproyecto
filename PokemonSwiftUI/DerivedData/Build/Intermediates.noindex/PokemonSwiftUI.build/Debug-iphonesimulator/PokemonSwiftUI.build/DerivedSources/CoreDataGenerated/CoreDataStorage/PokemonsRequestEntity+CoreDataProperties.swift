//
//  PokemonsRequestEntity+CoreDataProperties.swift
//  
//
//  Created by luis quitan on 3/08/21.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension PokemonsRequestEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PokemonsRequestEntity> {
        return NSFetchRequest<PokemonsRequestEntity>(entityName: "PokemonsRequestEntity")
    }

    @NSManaged public var page: Int32
    @NSManaged public var query: String?
    @NSManaged public var response: PokemonsResponseEntity?

}

extension PokemonsRequestEntity : Identifiable {

}
