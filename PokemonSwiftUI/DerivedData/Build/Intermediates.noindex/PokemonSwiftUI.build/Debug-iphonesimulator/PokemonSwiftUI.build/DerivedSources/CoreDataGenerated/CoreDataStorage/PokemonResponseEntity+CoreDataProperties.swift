//
//  PokemonResponseEntity+CoreDataProperties.swift
//  
//
//  Created by luis quitan on 3/08/21.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension PokemonResponseEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PokemonResponseEntity> {
        return NSFetchRequest<PokemonResponseEntity>(entityName: "PokemonResponseEntity")
    }

    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var url: String?
    @NSManaged public var pokemonsResponse: PokemonsResponseEntity?

}

extension PokemonResponseEntity : Identifiable {

}
